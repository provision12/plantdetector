package com.plant.android.plantdetector;

/**
 * Created by agamy on 3/29/2018.
 */

public class Plant {
    private String key;
    private String value;

    public Plant(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}