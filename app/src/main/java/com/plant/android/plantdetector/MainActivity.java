package com.plant.android.plantdetector;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    public static final int REQUEST_IMAGE_CAPTURE = 150;
    private static final int RC_CAMERA_AND_STORAGE = 200;
    ImageView mImageView;
    public Bitmap mBitmap;
    private String mCurrentPhotoPath;
    private Uri FilePathUri;
    String[] permsCameraAndStorage = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static Map<String,String> responseMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = findViewById(R.id.imageView);
    }

    public void captureImage(View view) {
        if (EasyPermissions.hasPermissions(this, permsCameraAndStorage)) {

            Toast.makeText(this, "Capture Image with Camera", Toast.LENGTH_SHORT).show();
            dispatchTakePictureIntent();

        } else {

            EasyPermissions.requestPermissions(this,"Camera and storage...",
                    RC_CAMERA_AND_STORAGE, permsCameraAndStorage);
        }
    }

    public void uploadImage(View view) {

        if(Utils.isInternetConnected(getBaseContext())) {
            new DownloadFilesTask().execute();
        }else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    public void openDetailsActivity(View view) {
            if(responseMap.size() >  0) {
                Intent intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
            }else {
                showDialog("Try upload vaild image again");
            }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

                Log.e("",ex.getLocalizedMessage());
            }

            if (photoFile != null) {
                FilePathUri = getUriFromPath(getBaseContext() , photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FilePathUri);
                takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public Uri getUriFromPath(Context context, File photoFile) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(context, context.getPackageName() + ".provider", photoFile);
        } else {
            return Uri.fromFile(photoFile);
        }
    }


    public Bitmap rotateBitmapByExif(Bitmap bitmap) {

        Matrix matrix = new Matrix();

        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }





        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                mBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                mBitmap= rotateBitmapByExif(mBitmap);
                mImageView.setImageBitmap(mBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getImageAsString() {
        String encodedBitmap = "";
        if (mBitmap != null) {
            encodedBitmap = Utils.encodeFileToBase64Binary(mBitmap);
        }
        return encodedBitmap;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    public Map<String, String> uploadImageToServer() {

        String SOAP_ACTION = "PlantDetectorSoapBinding";
        String METHOD_NAME = "detectPlantDetails ";
        String NAMESPACE = "http://plantdetection.opencv";

        String URL = Constants.SOAP_SERVICE_PATH;
        SoapObject resultString = null;

        try {
            SoapObject requestSoapObject = new SoapObject(NAMESPACE, METHOD_NAME);

            PropertyInfo propertyInfo =new PropertyInfo();
            propertyInfo.name = "imageStr";
            propertyInfo.setValue(getImageAsString());
            requestSoapObject.addProperty(propertyInfo);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = false;
            soapEnvelope.bodyOut = requestSoapObject;
            soapEnvelope.encodingStyle="Document";
            soapEnvelope.setOutputSoapObject(requestSoapObject);

            HttpTransportSE transport = new HttpTransportSE(URL);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapObject) soapEnvelope.bodyIn;

            Log.i("", "Result : " + resultString);

            if(resultString != null) {
                Object objectResponse = (Object) resultString.getProperty(0);
                SoapObject soapObject = (SoapObject) objectResponse;
                responseMap.clear();

                if (soapObject != null) {
                    int cols = soapObject.getPropertyCount();

                    for (int i = 0; i < cols; i++) {
                        PropertyInfo info = soapObject.getPropertyInfo(i);
                        responseMap.put(info.getName(), info.getValue().toString());

                    }
                }
                if (responseMap.size() > 0) {
                    String photoURL = String.format(Constants.IMAGE_SERVER_URL,responseMap.get(Constants.MAP_PLANT_ID));
                    responseMap.put(Constants.MAP_PLANT_URL, photoURL);
                }
            }



        } catch (Exception ex) {
            Log.e("", "Error: " + ex.getMessage());
        }

        return responseMap;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );


        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        if(requestCode == RC_CAMERA_AND_STORAGE)
            Toast.makeText(this, "all permissions granted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(requestCode == RC_CAMERA_AND_STORAGE)
            Toast.makeText(this, "all permissions denied", Toast.LENGTH_SHORT).show();
    }


    private class DownloadFilesTask extends AsyncTask<String, Void, Map<String, String>> {

        KProgressHUD kProgressHUD;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            kProgressHUD = KProgressHUD.create(MainActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait")
                    .setDetailsLabel("Processing image....")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();

        }

        @Override
        protected Map<String, String> doInBackground(String... strings) {

            return uploadImageToServer();
        }

        @Override
        protected void onPostExecute(Map<String, String> resultMap) {
            super.onPostExecute(resultMap);
            kProgressHUD.dismiss();
            if(responseMap.size() >  0) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                startActivity(intent);
            }else {
                showDialog("Try upload vaild image again");
            }
        }
    }

    private void showDialog(String result)
    {
        try {

            new MaterialDialog.Builder(MainActivity.this)
                    .title("Results")
                    .content(result)
                    .positiveText("OK")
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap createRotatedBitmap(String photoPath , Bitmap bitmap)
    {

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = 0;
        if (ei != null) {
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        }

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
                if(getManuFaturerName().equals("samsung"))
                {
                    rotatedBitmap = rotateImage(bitmap, 90);
                }
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        int width = source.getWidth();
        int height = source.getHeight();
        return Bitmap.createBitmap(source,0,0,width, height, matrix, true);
    }

    String getManuFaturerName()
    {
        return Build.MANUFACTURER;
    }


}//End Main Activity
