package com.plant.android.plantdetector;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {
    TextView resultTextView;
    ListView listView;
    ImageView imageViewPlant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        listView = findViewById(R.id.plant_listview);
        imageViewPlant = findViewById(R.id.plant_image_view);


        listView.setAdapter(new CustomPlantAdapter(this, getPlantList()));
        String plantPhotoURL = MainActivity.responseMap.get(Constants.MAP_PLANT_URL);

        Picasso.get().load(plantPhotoURL).into(imageViewPlant);

    }

    List<Plant> getPlantList() {
        // Here we get data from intent
        Log.e("", MainActivity.responseMap.toString());

        //Get Data from HashMap
        String plantDamage = MainActivity.responseMap.get(Constants.MAP_PLANT_DAMAGE);
        String plantId = MainActivity.responseMap.get(Constants.MAP_PLANT_ID);
        String plantLocation = MainActivity.responseMap.get(Constants.MAP_PLANT_LOCATION);
        String similarPlant = MainActivity.responseMap.get(Constants.MAP_PLANT_SIMILAR);
        String plantBenefit = MainActivity.responseMap.get(Constants.MAP_PLANT_BENEFIT);
        String plantName = MainActivity.responseMap.get(Constants.MAP_PLANT_NAME);
        String plantDescription = MainActivity.responseMap.get(Constants.MAP_PLANT_DESCRIPTION);

        String plantDamageAr = MainActivity.responseMap.get(Constants.MAP_PLANT_DAMAGE_AR);
        String plantLocationAr = MainActivity.responseMap.get(Constants.MAP_PLANT_LOCATION_AR);
        String similarPlantAr = MainActivity.responseMap.get(Constants.MAP_PLANT_SIMILAR_AR);
        String plantBenefitAr = MainActivity.responseMap.get(Constants.MAP_PLANT_BENEFIT_AR);
        String plantNameAr = MainActivity.responseMap.get(Constants.MAP_PLANT_NAME_AR);
        String plantDescriptionAr = MainActivity.responseMap.get(Constants.MAP_PLANT_DESCRIPTION_AR);

        //Create list here
        List<Plant> plantList=new ArrayList<>();
        //add data to list
        plantList.add(0, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_NAME).toString(), plantName));
        plantList.add(1, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_NAME_AR).toString(), plantNameAr));
        plantList.add(2, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_DESCRIPTION).toString(), plantDescription));
        plantList.add(3, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_DESCRIPTION_AR).toString(), plantDescriptionAr));
        plantList.add(4, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_LOCATION).toString(), plantLocation));
        plantList.add(5, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_LOCATION_AR).toString(), plantLocationAr));
        plantList.add(6, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_BENEFIT).toString(), plantBenefit));
        plantList.add(7, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_BENEFIT_AR).toString(), plantBenefitAr));
        plantList.add(8, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_DAMAGE).toString(), plantDamage));
        plantList.add(9, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_DAMAGE_AR).toString(), plantDamageAr));
        plantList.add(10, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_SIMILAR).toString(), similarPlant));
        plantList.add(11, new Plant(Constants.MAP_FIELD_LABEL.get(Constants.MAP_PLANT_SIMILAR_AR).toString(), similarPlantAr));

        return plantList;
    }

    public void backToMainActivity(View view) {
        switch (view.getId())
        {
            case R.id.back_image_view:
                super.onBackPressed();
                break;

        }
    }
}
