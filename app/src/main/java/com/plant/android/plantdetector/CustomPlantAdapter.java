package com.plant.android.plantdetector;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by agamy on 3/29/2018.
 */

public class CustomPlantAdapter extends ArrayAdapter<Plant> {


    public CustomPlantAdapter(@NonNull Context context, @NonNull List<Plant> objects) {
        super(context,0, objects);
    }

    private static class ViewHolder {
        TextView plantKey;
        TextView plantValueText;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //super.getView(position, convertView, parent);

        // Get the data item for this position
        Plant plant = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_text, parent, false);
            // Lookup view for data population
            viewHolder.plantKey = (TextView) convertView.findViewById(R.id.plantKey);
            viewHolder.plantValueText = (TextView) convertView.findViewById(R.id.plantValueText);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        }
        else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.plantKey.setText(plant.getKey());
        viewHolder.plantValueText.setText(plant.getValue());

        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


}
