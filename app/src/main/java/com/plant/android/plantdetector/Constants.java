package com.plant.android.plantdetector;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by agamy on 4/2/2018.
 */

public class Constants {

    public static final String TOMCAT_IP = "http://196.221.166.9:9090/";
    //public static final String TOMCAT_IP = "http://192.168.1.110:8080/";
    public static final String BASE_URL = TOMCAT_IP + "PlantDetectionService/";
    public static final String SOAP_SERVICE_PATH = BASE_URL + "services/PlantDetector?wsdl";
    public static final String SOAP_RESULTS_PATH = BASE_URL + "results/";
    public static final String IMAGE_EXTENSION = ".jpg";
    public static final String IMAGE_SERVER_URL = SOAP_RESULTS_PATH + "%s" + IMAGE_EXTENSION;


    //Here is server response constants
    public static final String MAP_PLANT_DAMAGE = "plantDamage";
    public static final String MAP_PLANT_ID = "plantId";
    public static final String MAP_PLANT_LOCATION = "plantLocation";
    public static final String MAP_PLANT_SIMILAR = "similarPlant";
    public static final String MAP_PLANT_BENEFIT = "plantBenefit";
    public static final String MAP_PLANT_NAME = "plantName";
    public static final String MAP_PLANT_DESCRIPTION = "plantDescription";
    public static final String MAP_PLANT_URL = "plantURL";
    public static final String MAP_PLANT_PHOTO = "plantPhoto";

    public static final String MAP_PLANT_DAMAGE_AR = "plantDamageAr";
    public static final String MAP_PLANT_LOCATION_AR = "plantLocationAr";
    public static final String MAP_PLANT_SIMILAR_AR = "similarPlantAr";
    public static final String MAP_PLANT_BENEFIT_AR = "plantBenefitAr";
    public static final String MAP_PLANT_NAME_AR = "plantNameAr";
    public static final String MAP_PLANT_DESCRIPTION_AR = "plantDescriptionAr";

    public static Map MAP_FIELD_LABEL = new HashMap<String, String>();
    static {
        MAP_FIELD_LABEL.put(MAP_PLANT_DAMAGE,"Plant Damage:");
        MAP_FIELD_LABEL.put(MAP_PLANT_ID,"Plant ID:");
        MAP_FIELD_LABEL.put(MAP_PLANT_LOCATION,"Plant Location:");
        MAP_FIELD_LABEL.put(MAP_PLANT_SIMILAR,"Similar Plant:");
        MAP_FIELD_LABEL.put(MAP_PLANT_BENEFIT,"Plant Benefit:");
        MAP_FIELD_LABEL.put(MAP_PLANT_NAME,"Plant Name:");
        MAP_FIELD_LABEL.put(MAP_PLANT_DESCRIPTION,"Plant Description:");
        MAP_FIELD_LABEL.put(MAP_PLANT_URL,"Plant URL:");
        MAP_FIELD_LABEL.put(MAP_PLANT_PHOTO,"");

        MAP_FIELD_LABEL.put(MAP_PLANT_DAMAGE_AR,"الأضرار:");
        MAP_FIELD_LABEL.put(MAP_PLANT_LOCATION_AR,"المناطق:");
        MAP_FIELD_LABEL.put(MAP_PLANT_SIMILAR_AR,"النباتات المشابهة:");
        MAP_FIELD_LABEL.put(MAP_PLANT_BENEFIT_AR,"الفوائد:");
        MAP_FIELD_LABEL.put(MAP_PLANT_NAME_AR,"الاسم:");
        MAP_FIELD_LABEL.put(MAP_PLANT_DESCRIPTION_AR,"الوصف:");
    }
}

